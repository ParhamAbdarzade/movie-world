package ir.urmis.domain.usecase

import io.reactivex.rxjava3.core.Single
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.repository.Repository

class GetMovieUseCaseImpl(private val repo: Repository) : GetMovieUseCase {
    override fun execute(id: String): Single<MovieDomainModel> = repo.getMovie(id)
}

interface GetMovieUseCase {
    fun execute(id: String): Single<MovieDomainModel>
}
