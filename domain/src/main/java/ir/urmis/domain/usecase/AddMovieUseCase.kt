package ir.urmis.domain.usecase

import io.reactivex.rxjava3.core.Single
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.repository.Repository

interface AddMovieUseCase {

    fun execute(movie: MovieDomainModel): Single<Boolean>
}

class AddMovieUseCaseImpl(private val repository: Repository) : AddMovieUseCase {
    override fun execute(movie: MovieDomainModel): Single<Boolean> = repository.addMovie(movie)
}