package ir.urmis.domain.usecase

import io.reactivex.rxjava3.core.Single
import ir.urmis.domain.model.SearchDomainModel
import ir.urmis.domain.repository.Repository

interface SearchMovieUseCase {

    fun execute(title: String): Single<SearchDomainModel>
}

class SearchMovieUseCaseImpl(private val repository: Repository) : SearchMovieUseCase {
    override fun execute(title: String) = repository.searchMovie(title)
}