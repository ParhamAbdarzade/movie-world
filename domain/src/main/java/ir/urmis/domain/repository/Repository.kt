package ir.urmis.domain.repository

import io.reactivex.rxjava3.core.Single
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.model.SearchDomainModel

interface Repository {

    fun getMovie(id: String): Single<MovieDomainModel>

    fun searchMovie(title: String): Single<SearchDomainModel>

    fun addMovie(movie: MovieDomainModel): Single<Boolean>
}