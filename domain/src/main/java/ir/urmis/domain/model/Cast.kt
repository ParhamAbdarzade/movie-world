package ir.urmis.domain.model

data class Cast(
    val actor: String?,
    val actor_id: String,
    val character: String?
)