package ir.urmis.domain.model

data class MovieDomainModel(
    val id: String,
    val title: String?,
    val poster: String?,
    val rating: String?,
    val cast: List<Cast>?,
    val length: String?,
    val plot: String?,
    val rating_votes: String?,
    val year: String?
)
