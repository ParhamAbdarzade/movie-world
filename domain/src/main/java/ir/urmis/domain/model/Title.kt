package ir.urmis.domain.model

data class Title(
    val id: String,
    val image: String?,
    val title: String
)