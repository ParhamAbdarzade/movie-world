package ir.urmis.domain.di

import ir.urmis.domain.usecase.*
import org.koin.dsl.module

val useCaseImplModule =
    module {
        single<GetMovieUseCase>(createdAtStart = false) { GetMovieUseCaseImpl(get()) }
        single<SearchMovieUseCase>(createdAtStart = false) { SearchMovieUseCaseImpl(get()) }
        single<AddMovieUseCase>(createdAtStart = false) { AddMovieUseCaseImpl(get()) }
    }
