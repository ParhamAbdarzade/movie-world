package ir.urmis.movieworld

import androidx.multidex.MultiDexApplication
import ir.urmis.data.di.dataModule
import ir.urmis.data.di.networkModule

import ir.urmis.domain.di.useCaseImplModule
import ir.urmis.movieworld.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieWorldApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MovieWorldApp)
            modules(listOf(viewModelModule , useCaseImplModule , dataModule , networkModule))
        }
    }
}