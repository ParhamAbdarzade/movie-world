package ir.urmis.movieworld.di

import ir.urmis.movieworld.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { MainViewModel(get(), get(), get(), get()) }
}