package ir.urmis.movieworld.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

import java.lang.ref.WeakReference

open class BaseViewModel<N> : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    private var mNavigator: WeakReference<N>? = null

    fun addToDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun getNavigator() = mNavigator?.get()

    fun setNavigator(navigator: N) {
        mNavigator = WeakReference(navigator)
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}