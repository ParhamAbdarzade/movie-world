package ir.urmis.movieworld.ui.main

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.movieworld.R
import ir.urmis.movieworld.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class MainActivity : AppCompatActivity(), MainNavigator {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.vm = getViewModel()
        binding.vm!!.setNavigator(this)
        binding.vm!!.getMovie("tt1375666")

    }

    override fun done(movieDomainModel: MovieDomainModel) {
        Toast.makeText(this, movieDomainModel.id, Toast.LENGTH_LONG).show()

    }
}