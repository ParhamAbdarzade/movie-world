package ir.urmis.movieworld.ui.main

import ir.urmis.domain.model.MovieDomainModel

interface MainNavigator {

    fun done(movieDomainModel: MovieDomainModel)
}