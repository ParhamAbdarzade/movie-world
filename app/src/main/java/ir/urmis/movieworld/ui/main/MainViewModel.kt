package ir.urmis.movieworld.ui.main

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.usecase.AddMovieUseCase
import ir.urmis.domain.usecase.GetMovieUseCase
import ir.urmis.domain.usecase.SearchMovieUseCase
import ir.urmis.movieworld.ui.base.BaseViewModel
import ir.urmis.movieworld.utlis.NotNullMutableLiveData
import ir.urmis.movieworld.utlis.with
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.adapter.rxjava3.HttpException

class MainViewModel(
    private val getMovieUseCase: GetMovieUseCase,
    private val searchMovieUseCase: SearchMovieUseCase,
    private val addMovieUseCase: AddMovieUseCase, private val context: Context
) :
    BaseViewModel<MainNavigator>() {


    private val _refreshing: NotNullMutableLiveData<Boolean> = NotNullMutableLiveData(false)
    val refreshing: NotNullMutableLiveData<Boolean>
        get() = _refreshing

    private val _items: MutableLiveData<Any> = MutableLiveData()
    val items: MutableLiveData<Any> get() = _items

    fun getMovie(id: String) {


        addToDisposable(
            getMovieUseCase.execute(id).with()
                .doOnSubscribe {
                    _refreshing.value = true
                }
                .doOnError {
                    _refreshing.value = false
                    var responseBody: ResponseBody?
                    if (it is HttpException) {
                        responseBody = it.response()?.errorBody()

                        // TODO
                        // I don't know what should I do on error
                        if (responseBody != null) {
                            val jsonObject = JSONObject(responseBody.string())

                            if (!jsonObject.getString("message").isNullOrBlank())
                                println(jsonObject.getString("message"))
                        }
                    }
                }
                .doOnSuccess {
                    _refreshing.value = false
                }.subscribe({

                    val cast = if (it.cast != null) "api" else "db"
                    toast("got it from $cast")
                    addMovie(it)

                }, {

                })
        )
    }

    fun searchMovie(title: String) {


        addToDisposable(
            searchMovieUseCase.execute(title).with()
                .doOnSubscribe {
                    _refreshing.value = true
                }
                .doOnError {
                    _refreshing.value = false
                    var responseBody: ResponseBody?
                    if (it is HttpException) {
                        responseBody = it.response()?.errorBody()

                        // TODO
                        // I don't know what should I do on error
                        if (responseBody != null) {
                            val jsonObject = JSONObject(responseBody.string())

                            if (!jsonObject.getString("message").isNullOrBlank())
                                println(jsonObject.getString("message"))
                        }
                    }
                }
                .doOnSuccess {
                    _refreshing.value = false
                }.subscribe({

//                    getNavigator()?.done(it)

                }, {

                })
        )
    }

    fun addMovie(movie: MovieDomainModel) {
        addMovieUseCase.execute(movie).with().doOnSubscribe {
            _refreshing.value = true
        }
            .doOnError {
                _refreshing.value = false
                toast(it.message.toString())
            }
            .doOnSuccess {
                _refreshing.value = false
            }.subscribe({
                toast("done")
            }, {

            })
    }

    fun toast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }
}