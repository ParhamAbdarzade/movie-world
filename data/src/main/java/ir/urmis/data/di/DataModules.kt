package ir.urmis.data.di

import ir.urmis.data.local.db.MyDB
import ir.urmis.data.local.db.MyDBNavigator
import ir.urmis.data.mapper.MapperImpl
import ir.urmis.data.remote.apiservice.ApiService
import ir.urmis.data.repo.RepositoryImpl
import ir.urmis.data.utlis.Constants
import ir.urmis.domain.repository.Repository
import org.koin.dsl.module
import retrofit2.Retrofit

val dataModule = module {
    single(createdAtStart = true) { MapperImpl() }
    single<MyDBNavigator>(createdAtStart = true) {
        MyDB(get(), Constants.DBNAME, null, 1)
    }
    single<ApiService>(createdAtStart = false) { get<Retrofit>().create(ApiService::class.java) }
    single<Repository>(createdAtStart = false) { RepositoryImpl(get(), get(), get()) }
}
