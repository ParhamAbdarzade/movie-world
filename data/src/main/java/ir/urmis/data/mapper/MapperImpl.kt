package ir.urmis.data.mapper

import ir.urmis.data.model.Cast
import ir.urmis.data.model.MovieDataModel
import ir.urmis.data.model.SearchDataModel
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.model.SearchDomainModel
import ir.urmis.domain.model.Title

class MapperImpl : Mapper {

    override fun toMovieModel(movie: MovieDataModel): MovieDomainModel = MovieDomainModel(
        id = movie.id,
        title = movie.title,
        poster = movie.poster,
        rating = movie.rating,
        cast = toCastModel(movie.cast),
        length = movie.length,
        plot = movie.plot,
        rating_votes = movie.rating_votes,
        year = movie.year
    )

    override fun toCastModel(cast: List<Cast>?): List<ir.urmis.domain.model.Cast>? {

        if (cast == null)
            return null
        val outCast = ArrayList<ir.urmis.domain.model.Cast>(cast.size)

        cast.forEach {
            outCast.add(ir.urmis.domain.model.Cast(it.actor, it.actor_id, it.character))
        }

        return outCast
    }

    override fun toSearchModel(search: SearchDataModel): SearchDomainModel {

        if (search.names.isNullOrEmpty())
            return SearchDomainModel(null)

        val titles = ArrayList<Title>()

        search.names.forEach {
            titles.add(Title(it!!.id, it.image, it.title))
        }

        return SearchDomainModel(titles)
    }
}