package ir.urmis.data.mapper

import ir.urmis.data.model.Cast
import ir.urmis.data.model.MovieDataModel
import ir.urmis.data.model.SearchDataModel
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.model.SearchDomainModel

interface Mapper {

    fun toMovieModel(movie: MovieDataModel): MovieDomainModel

    fun toCastModel(cast: List<Cast>?): List<ir.urmis.domain.model.Cast>?

    fun toSearchModel(search: SearchDataModel): SearchDomainModel
}