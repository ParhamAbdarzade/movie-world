package ir.urmis.data.local.db

import io.reactivex.rxjava3.core.Single
import ir.urmis.data.model.MovieDataModel
import ir.urmis.domain.model.MovieDomainModel

interface MyDBNavigator {

    fun getMovie(id: String): Single<MovieDataModel>?

    fun addMovie(model: MovieDomainModel): Single<Boolean>
}