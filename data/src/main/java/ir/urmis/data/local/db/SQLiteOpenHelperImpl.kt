package ir.urmis.data.local.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ir.urmis.data.utlis.Constants

open class SQLiteOpenHelperImpl(
    context: Context,
    name: String,
    cursor: SQLiteDatabase.CursorFactory?,
    version: Int
) :
    SQLiteOpenHelper(context, name, cursor, version) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(Constants.create_film_table)
        db?.execSQL(Constants.create_cast_table)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        return
    }

}
