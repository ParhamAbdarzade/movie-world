package ir.urmis.data.local.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import io.reactivex.rxjava3.core.Single
import ir.urmis.data.model.Cast
import ir.urmis.data.model.MovieDataModel
import ir.urmis.data.utlis.Constants
import ir.urmis.domain.model.MovieDomainModel

class MyDB(
    context: Context,
    name: String,
    cursor: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelperImpl(context, name, cursor, version), MyDBNavigator {

    override fun getMovie(id: String): Single<MovieDataModel>? {

        val query = "SELECT * FROM ${Constants.film_table} WHERE ${Constants.film_id} = '$id' ; "

        val cursor = readableDatabase.rawQuery(query, null)

        cursor.moveToFirst()
        if (cursor.isFirst) {
            val movie =
                MovieDataModel(
                    cast = getCast(filmID = id),
                    id = cursor.getString(cursor.getColumnIndex(Constants.film_id)),
                    rating = cursor.getString(cursor.getColumnIndex(Constants.rating)),
                    rating_votes = cursor.getString(cursor.getColumnIndex(Constants.rating_votes)),
                    poster = cursor.getString(cursor.getColumnIndex(Constants.poster)),
                    plot = cursor.getString(cursor.getColumnIndex(Constants.plot)),
                    year = cursor.getString(cursor.getColumnIndex(Constants.year)),
                    length = cursor.getString(cursor.getColumnIndex(Constants.length)),
                    title = cursor.getString(cursor.getColumnIndex(Constants.title)),
                    technical_specs = null,
                    trailer = null
                )

            cursor.close()
            return Single.create {
                it.onSuccess(movie)
            }
        } else return null
    }

    override fun addMovie(model: MovieDomainModel): Single<Boolean> {


        val film = ContentValues()
        film.apply {

            put(Constants.film_id, model.id)

            if (model.rating != null)
                put(Constants.rating, model.rating)
            if (model.rating_votes != null)
                put(Constants.rating_votes, model.rating_votes)

            if (model.title != null)
                put(Constants.title, model.title)

            if (model.length != null)
                put(Constants.length, model.length)

            if (model.plot != null)
                put(Constants.plot, model.plot)

            if (model.year != null)
                put(Constants.year, model.year)
        }
        writableDatabase.close()
        val op: Long = writableDatabase.insert(Constants.film_table, null, film)

        if (op == -1L)
            return Single.create {
                it.onError(object : Throwable() {
                    override val message: String
                        get() = "DB INSERT ERROR"
                })
            }

        if (!model.cast.isNullOrEmpty()) {
            model.cast!!.forEach {
                val cast = ContentValues()
                cast.apply {
                    put(Constants.film_id, model.id)
                    put(Constants.actor_id, it.actor_id)
                    put(Constants.character, it.character)
                    put(Constants.actor, it.actor)
                }
                writableDatabase.insert(Constants.cast_table, null, cast)
            }
        }

        return Single.create {
            it.onSuccess(true)
        }
    }

    private fun getCast(filmID: String): List<Cast> {
        val cast = ArrayList<Cast>()
        val query =
            "SELECT * FROM ${Constants.cast_table} WHERE ${Constants.film_id} = '$filmID' ; "

        val cursor = readableDatabase.rawQuery(query, null)

        cursor.moveToFirst()
        if (cursor.isFirst) {
            while (!cursor.isAfterLast) {
                val newCast =
                    Cast(
                        actor = cursor.getString(cursor.getColumnIndex(Constants.actor)),
                        actor_id = cursor.getString(cursor.getColumnIndex(Constants.actor_id)),
                        character = cursor.getString(cursor.getColumnIndex(Constants.character)),
                    )

                cast.add(newCast)
                cursor.moveToNext()
            }
        }
        cursor.close()
        return cast
    }
}