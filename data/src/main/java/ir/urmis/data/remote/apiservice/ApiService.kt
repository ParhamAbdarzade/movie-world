package ir.urmis.data.remote.apiservice

import io.reactivex.rxjava3.core.Single
import ir.urmis.data.model.MovieDataModel
import ir.urmis.data.model.SearchDataModel
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("/film/{id}")
    fun getMovie(@Path("id") id: String): Single<MovieDataModel>

    @GET("/search/{query}")
    fun searchMovie(@Path("queryd") title: String): Single<SearchDataModel>
}