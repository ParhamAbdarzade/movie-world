package ir.urmis.data.remote.apiservice

object URL {

    const val BASE_URL = "https://imdb-internet-movie-database-unofficial.p.rapidapi.com"
}