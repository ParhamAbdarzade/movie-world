package ir.urmis.data.model

data class Cast(
    val actor: String,
    val actor_id: String,
    val character: String
)