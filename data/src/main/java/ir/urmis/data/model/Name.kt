package ir.urmis.data.model

data class Name(
    val id: String,
    val image: String?,
    val title: String
)