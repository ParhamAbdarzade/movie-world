package ir.urmis.data.model

data class Title(
    val id: String,
    val image: String,
    val title: String
)