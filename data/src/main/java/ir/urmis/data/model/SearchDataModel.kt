package ir.urmis.data.model

data class SearchDataModel(
    val companies: List<Company?>?,
    val names: List<Name?>?,
    val titles: List<Title?>?
)