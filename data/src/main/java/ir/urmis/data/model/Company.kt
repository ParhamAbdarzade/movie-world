package ir.urmis.data.model

data class Company(
    val id: String,
    val image: Any,
    val title: String
)