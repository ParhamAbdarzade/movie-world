package ir.urmis.data.model

data class Trailer(
    val id: String,
    val link: String
)