package ir.urmis.data.utlis

object Constants {

    const val DBNAME = "MOVIE_WORLD_DB"
    // TABLE NAMES
    const val film_table = "FILM_TABLE"
    const val cast_table = "CAST_TABLE"
    const val top_films_table = "TOP_FILMS_TABLE"
    const val popular_films_table = "POPULAR_FILMS_TABLE"

    // FILM TABLE

    const val title = "TITLE"
    const val rating = "RATING"
    const val film_id = "FILM_ID"
    const val poster = "POSTER"
    const val length = "LENGTH"
    const val plot = "PLOT"
    const val rating_votes = "RATING_VOTES"
    const val year = "YEAR"

    const val create_film_table = "CREATE TABLE IF NOT EXISTS $film_table (" +
            " $film_id TEXT PRIMARY KEY , " +
            "$title TEXT NOT NULL , " +
            "$poster TEXT , " +
            "$length TEXT , " +
            "$plot TEXT , " +
            "$rating_votes TEXT , " +
            "$rating TEXT , " +
            "$year TEXT  " +
            ");"
    // CAST TABLE

    const val actor = "ACTOR"
    const val actor_id = "ACTOR_ID"
    const val character = "CHARACTER"

    const val create_cast_table = "CREATE TABLE IF NOT EXISTS $cast_table (" +
            " $film_id TEXT NOT NULL , " +
            "$actor_id TEXT NOT NULL , " +
            "$actor TEXT  , " +
            "$character TEXT , " +
            "PRIMARY KEY ($film_id , $actor_id) , " +
            "FOREIGN KEY ($film_id) " +
            "REFERENCES $film_table ($film_id)" +
            " ON UPDATE NO ACTION" +
            " ON DELETE CASCADE"+
            ");"
}
