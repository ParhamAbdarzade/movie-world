package ir.urmis.data.repo

import io.reactivex.rxjava3.core.Single
import ir.urmis.data.local.db.MyDBNavigator
import ir.urmis.data.mapper.MapperImpl
import ir.urmis.data.remote.apiservice.ApiService
import ir.urmis.domain.model.MovieDomainModel
import ir.urmis.domain.model.SearchDomainModel
import ir.urmis.domain.repository.Repository


class RepositoryImpl(
    val myDBNavigator: MyDBNavigator,
    val apiService: ApiService,
    private val mapper: MapperImpl
) : Repository {

    override fun getMovie(id: String): Single<MovieDomainModel> {

        val data = myDBNavigator.getMovie(id)
        if (data != null)
            return data.map { mapper.toMovieModel(it) }

        return apiService.getMovie(id).map {
            mapper.toMovieModel(it)
        }
    }

    override fun searchMovie(title: String): Single<SearchDomainModel> =
        apiService.searchMovie(title).map {
            mapper.toSearchModel(it)
        }

    override fun addMovie(movie: MovieDomainModel): Single<Boolean> = myDBNavigator.addMovie(movie)
}